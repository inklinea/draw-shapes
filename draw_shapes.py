#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
# #############################################################################
#  Draw Shapes - An Inkscape Extension
#  Appears in 'Extensions>Render
#  Just a simple example to make a <rect>, <circle, <path> in Inkscape
#  Requires Inkscape 1.1+ -->
# #############################################################################

import inkex
import random
from lxml import etree

def random_rgb(self):
    random_red = random.randrange(0, 255)
    random_green = random.randrange(0, 255)
    random_blue = random.randrange(0, 255)

    return f'rgb({random_red}, {random_green}, {random_blue})'

# Could not find simplestyle, found this instead in extensions repo
def format_style(my_style):
    """Format an inline style attribute from a dictionary"""
    return ";".join([att + ":" + str(val) for att, val in my_style.items()])

def make_rect(self, parent, x, y, width, height):

    random_rgb_fill = random_rgb(self)
    random_rgb_stroke = random_rgb(self)

    style = {'stroke': f'{random_rgb_stroke}',
             'stroke-width': '1',
             'fill': f'{random_rgb_fill}',
             # 'stroke-dasharray': '1,1'
             }

    attribs = {
        'style': format_style(style),
        'x': str(x),
        'y': str(y),
        'width': str(width),
        'height': str(height)
    }

    rect = etree.SubElement(parent, inkex.addNS('rect', 'svg'), attribs)
    return rect

def make_circle(self, parent, cx, cy, r):
    random_rgb_fill = random_rgb(self)
    random_rgb_stroke = random_rgb(self)

    # random_rgb_color = 'black'

    style = {'stroke': f'{random_rgb_stroke}',
             'stroke-width': '1',
             'fill': f'{random_rgb_fill}',
             # 'stroke-dasharray': '1,1'
             }

    attribs = {
        'style': format_style(style),
        'cx': str(cx),
        'cy': str(cy),
        'r': str(r)
    }

    circle = etree.SubElement(parent, inkex.addNS('circle', 'svg'), attribs)
    return circle

def make_path(self, parent, x1, y1, x2, y2):

    random_rgb_stroke = random_rgb(self)
    random_dasharray = f'{random.randrange(1,5)}, {random.randrange(1,5)}'

    style = {'stroke': f'{random_rgb_stroke}',
             'stroke-width': '1',
             'stroke-dasharray': str(random_dasharray)
             }

    attribs = {
        'style': format_style(style),
        'd': f'M {x1} {y1} {x2} {y2}z'
    }

    path = etree.SubElement(parent, inkex.addNS('path', 'svg'), attribs)
    return path

class DrawShapes(inkex.EffectExtension):

    def effect(self):

        parent = self.svg

        make_rect(self, parent, 10, 10, 30, 40)
        make_circle(self, parent, 100, 100, 30)
        make_path(self, parent, 10, 150, 200, 230)


if __name__ == '__main__':
    DrawShapes().run()
